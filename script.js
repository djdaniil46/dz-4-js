"use strict";

const createNewUser = () =>{
const newUser = {
    firstName: prompt("Введите имя"),
    lastName: prompt("Введите фамилию"),
    birthday: new Date(prompt("Укажите дату Вашего рождения в формате 'dd.mm.yyyy'")),
    getAge(){
        let today = new Date();
        let birthDate = this.birthday;
        let age = today.getFullYear() - birthDate.getFullYear();
        let m = today.getMonth() - birthDate.getMonth();
        
             if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
        };
    return age;
    },
    getPassword(){
        return(this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthday.getFullYear());
    },
    getLogin(){
        return(this.firstName[0].toLowerCase()+this.lastName.toLowerCase());
    },
};

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
return newUser;
};

createNewUser();

